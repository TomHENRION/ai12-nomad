# AI12 - Nomad

## Makefile

The Makefile will automatically handle the virtual env. Type `make` to see the available commands.

## Setup git

1. [Add an ssh key to your gitlab account](https://docs.gitlab.com/ee/ssh/)
2. `git clone git@gitlab.com:0ggar/ai12-nomad.git`
3. [Fork the repository](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork)
4. Add the forked repository as a remote `git remote add fork  git@gitlab.com:{user_name}/ai12-nomad.git`
5. Consider disabling push on `origin`: `git remote set-url --push origin no_push`

## Contribute

1. To make changes start with creating a branch `git checkout -b my_feature origin/main`.
2. Push to your fork using the same branch name. `git push fork my_feature`
3. [Create a Merge Request on gitlab](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-use-git-commands-locally)
